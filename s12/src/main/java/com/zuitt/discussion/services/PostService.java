package com.zuitt.discussion.services;


import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    //Create post
    void createPost(Post post);
    //Viewing all post
    Iterable<Post> getPosts();
    //Delete a post
    ResponseEntity deletePost(Long id);
    // Edit post
    ResponseEntity updatePost(Long id,Post post);



}
