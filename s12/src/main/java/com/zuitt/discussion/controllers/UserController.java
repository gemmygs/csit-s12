package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;
    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User create successfully.", HttpStatus.CREATED);
    }
    @GetMapping("/users")
    public ResponseEntity<Object>getAllUser(){
        return new ResponseEntity<>(userService.getAllUser(),HttpStatus.OK);
    }
    @DeleteMapping("/users/{userid}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid){
        return userService.deleteUser(userid);
    }
    @PutMapping("/users/{userid}")
    public ResponseEntity<Object> updateUser(@RequestBody User user, @PathVariable Long userid){
        return userService.updateUser(userid, user);
    }
}
